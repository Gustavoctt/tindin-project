import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';

const AUTH_API = 'https://h-api-ava.tindin.com.br/';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})

export class AuthService{
  constructor(private http: HttpClient){}

  login(email: string, password: string): Observable<any>{
    return this.http.post(AUTH_API + 'auth', {
      email,
      password
    }, httpOptions)
  }


}