import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
// import { Quizzes } from "../interfaces/quizzes.interface";
import { QuizzesResponse } from "../interfaces/quizzesResponse.interface";


const baseUrl = 'https://h-api-ava.tindin.com.br/quizzes'

@Injectable({
  providedIn: 'root'
})

export class QuizzesService{
  constructor(private http: HttpClient){}

  getAll(): Observable<QuizzesResponse>{
    return this.http.get<QuizzesResponse>(`${baseUrl}?filter=team:623497e07ccb72a54717b9f4&fields=title, description, level, rewardXp, options`);
  }

  delete(id: any): Observable<any>{
    return this.http.delete(`${baseUrl}/quizzes/${id}`)
  }

  create(data: any): Observable<any>{
    return this.http.post(baseUrl, data);
  }
}