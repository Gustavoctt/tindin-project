import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Quizzes } from "../interfaces/quizzes.interface";
import { QuizzesService } from "../services/quizzes.service";
import { TokenStorageService } from "../services/token-storage.service";

@Component({
  selector: 'app-create-question',
  templateUrl: './createQuestion.component.html',
  styleUrls: ['./createQuestion.component.css']
})

export class createQuestionComponent implements OnInit{
  currentUser: any;
  
  quizzes: Quizzes = {
    team: '',
    title: '',
    description: '',
    level: '',
    type: '',
    rewardXp: 0,
    options: []
  }

  public options: any[] = [{
  }]

  constructor( private token: TokenStorageService, private quizzesService:QuizzesService, private router: Router){}

  ngOnInit(): void {
    this.currentUser = this.token.getToken()
  }

  saveQuiz(): void{
    const data = {
      team: '623497e07ccb72a54717b9f4',
      title: this.quizzes.title,
      description: this.quizzes.description,
      level: this.quizzes.level,
      type: this.quizzes.type,
      rewardXp: this.quizzes.rewardXp,
      options: this.quizzes.options
    }

    console.log(data)

    // this.quizzesService.create(data).subscribe(
    //   response => console.log(response),
    //   error => console.log(error)
    // )
  }

  addOption(){
    this.options.push({
      correct: false,
      text: ''
    });

    console.log(this.options)
  }

  
  

}