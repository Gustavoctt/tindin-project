import { QuizzesOptions } from "./quizzesOptions.interface";

export interface Quizzes{
  team: string;
  title: string;
  description ?: string;
  level ?: string;
  rewardXp ?: number;
  type?: string;
  _id?: string;
  options?: QuizzesOptions[];
  
}

