import { Quizzes } from "./quizzes.interface";

export interface QuizzesResponse{
  quizzes: Quizzes[];
  totalSize: number;
}