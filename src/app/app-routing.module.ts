import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { createQuestionComponent } from './createQuestion/createQuestion.component';
import { HomeQuiz } from './homeQuiz/homeQuiz.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'home', component: HomeQuiz},
  { path: 'create-question', component: createQuestionComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
