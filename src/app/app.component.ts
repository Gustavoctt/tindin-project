import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'tindin';

  isLoggedIn = false;
  showHomePage = false;

  constructor(private tokenStorageService: TokenStorageService){}

  ngOnInit(): void {
      this.isLoggedIn = !!this.tokenStorageService.getToken();
  }

}
