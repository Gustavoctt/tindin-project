import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Quizzes } from "../interfaces/quizzes.interface";

import { QuizzesService } from "../services/quizzes.service";
import { TokenStorageService } from "../services/token-storage.service";

@Component({
  selector: 'app-home-quiz',
  templateUrl: './homeQuiz.component.html',
  styleUrls: ['./homeQuiz.component.css']
})

export class HomeQuiz implements OnInit{
  currentUser: any;
 

  quizzes ?: Quizzes[];

  constructor(private token: TokenStorageService, private quizzesService: QuizzesService,  private router: Router){}

  ngOnInit(): void {
      this.currentUser = this.token.getToken()

      this.retriveQuizzes();
  }

  retriveQuizzes(): void{
    this.quizzesService.getAll().subscribe(
      data => {
        this.quizzes = data.quizzes;
        console.log(data)
      },
      error => {
        console.log(error)
      }
    )
  }

  deleteQuiz(): void{
    // console.log(this.quiz._id)
    
    // this.quizzesService.delete(this.quizzes).subscribe(
    //   response => {
    //     console.log(response);
    //   }
    // )
  }

  addQuiz(): void{
    this.router.navigateByUrl('/create-question');
  }
  
  
}