# Tindin

Esse projeto foi feito na versão 13.2.5.

## Development server

Rode `ng serve` para o server de dev. Navegue até `http://localhost:4200/`.

## Sobre mim

Nome: Gustavo Tartare <br>
Email: gustavocarrertartare@gmail.com <br>
Fone: (48) 99950-2728 <br>
Linkedin: https://www.linkedin.com/in/gustavo-tartare/

## Sobre o Projeto

Nesse desafio tive a oportunidade de entrar mais a fundo no universo Angular, até então nunca tinha feito nenhum projeto. Consegui abordar várias teorias, como o Router, Autenticação e Formulários. <br>
Não consegui finalizar o projeto, pois o prazo era curto para a entrega, e como não tinha trabalhado fazendo um CRUD completo, senti uma dificuldade (conforme ia surgindo as dúvidas eu ia pesquisando).<br>
Mas agradeço a oportunidade e com certeza vou finalizar o projeto, pois nesse tempo em que estive fazendo aprendi vários conceitos para a minha carreira como Dev.
